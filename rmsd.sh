#! /bin/bash

# RMSD se define como
# SQRT(1/N SUM(dD^2)), donde dD es la diferencia de posiciones
# dD va a tener valores para x, y, z.
# dDx=SUM(xi-x0), donde xi es la posición de un atomo y x0 es la posición del mismo atomo en otra estructura

#original

#@<TRIPOS>MOLECULE
#225.2NNJ.A.pdb
# 25 26 0 0 0
#SMALL
#GASTEIGER
#
#@<TRIPOS>ATOM
#      1  C         48.8130   13.6000  -24.7710 C.ar    1  UNL1        0.0880
#      2  C         48.8170   14.8910  -23.9180 C.ar    1  UNL1        0.0150


#docking

#@<TRIPOS>MOLECULE
#225.2NNJ.A.pdb
#25 26 0 0 0
#SMALL
#GASTEIGER
#
#@<TRIPOS>ATOM
#      1  C         48.6270   13.2800  -24.3360 C.ar    1  UNL1        0.0880
#      2  C         47.8910   14.1260  -25.4020 C.ar    1  UNL1        0.0150

output=$1
original=$2

if [ -f "${output}" ] && [ -f "${original}" ] ; then

#no puedo sacarme de encima el usar archivos temporales

sed -n '/@<TRIPOS>ATOM/,/@<TRIPOS>BOND/{/@<TRIPOS>ATOM/b;/@<TRIPOS>BOND/b;p}' "${original}" >> XYZ_orig.temp    #coordenadas del cristal
sed -n '/@<TRIPOS>ATOM/,/@<TRIPOS>BOND/{/@<TRIPOS>ATOM/b;/@<TRIPOS>BOND/b;p}' "${output}" >> XYZ_output.temp         #coordenadas del docking

awk '{print $3}' "XYZ_orig.temp" > XO.temp        #columna de X del cristal
awk '{print $3}' "XYZ_output.temp" > Xi.temp      #columna de X del docking
paste Xi.temp XO.temp > X.temp                    #archivo con ambas columnas "paste" es el comando que sirve para eso

awk '{print $4}' "XYZ_orig.temp" > YO.temp
awk '{print $4}' "XYZ_output.temp" > Yi.temp
paste Yi.temp YO.temp > Y.temp

awk '{print $5}' "XYZ_orig.temp" > ZO.temp
awk '{print $5}' "XYZ_output.temp" > Zi.temp

paste Zi.temp ZO.temp > Z.temp

#lectura de los archivos anteriores
while read xatom; do

XA=$(awk '{print $1}' <<< $xatom)
XB=$(awk '{print $2}' <<< $xatom)
DX2=$(echo "( $XA - $XB )*( $XA - $XB )" | bc -l)
echo $DX2 >> XDX2.temp

done < X.temp

while read yatom; do

YA=$(awk '{print $1}' <<< $yatom)
YB=$(awk '{print $2}' <<< $yatom)
DY2=$(echo "( $YA - $YB )*( $YA - $YB )" | bc -l)
echo $DY2 >> YDY2.temp

done < Y.temp

while read zatom; do

ZA=$(awk '{print $1}' <<< $zatom)
ZB=$(awk '{print $2}' <<< $zatom)
DZ2=$(echo "( $ZA - $ZB )*( $ZA - $ZB )" | bc -l)
echo $DZ2 >> ZDZ2.temp

done < Z.temp

SUMDX2=$(awk '{ sum += $1 } END { print sum / NR }' XDX2.temp)
SUMDY2=$(awk '{ sum += $1 } END { print sum / NR }' YDY2.temp)
SUMDZ2=$(awk '{ sum += $1 } END { print sum / NR }' ZDZ2.temp)

RMSD=$(echo " sqrt( $SUMDX2 + $SUMDY2 + $SUMDZ2 ) " | bc -l)

echo "$RMSD"

#clean temps
rm *.temp

else

echo "No se pueden encontrar los archivos ${output} y ${original}"

exit 1

fi
