#! /bin/bash

coords="$1"

if [ -f "${coords}" ] ; then

while read line; do
  # cada par de valores en $line

PDBID=$(tr -d " " <<< $(awk -F',' '{print $1}' <<< "$line")) #nombre del pdb
LIG=$(tr -d " " <<< $(awk -F',' '{print $2}' <<< "$line")) #nombre del ligando

if [ -f "${LIG}.${PDBID}.A.pdbqt" ] && [ -f "${PDBID}.pdbqt" ] ; then

XCOOR=$(awk -F',' '{print $3}' <<< "$line") #x
YCOOR=$(awk -F',' '{print $4}' <<< "$line") #y
ZCOOR=$(awk -F',' '{print $5}' <<< "$line") #z

/usr/bin/vina --receptor "${PDBID}.pdbqt" --ligand "${LIG}.${PDBID}.A.pdbqt" \
              --center_x $XCOOR --center_y $YCOOR --center_z $ZCOOR \
              --size_x 15 --size_y 15 --size_z 15 \
              --out "${LIG}.${PDBID}.A_out.pdbqt"

#obtener valores de energía

#MODEL 1
#REMARK VINA RESULT:      -9.2      0.000      0.000
#REMARK  Name = 11D.3AKM.A.pdb

lineaDG=$(sed '2q;d' "${LIG}.${PDBID}.A_out.pdbqt")
DG=$(awk '{print $4}' <<< "$lineaDG")
DGJ=$(echo "$DG * 4.184" | bc -l)

echo "${PDBID} , ${LIG} , ${DGJ}" >> energia.csv

fi

fi

else

echo "No se encuentra el archivo de coordenadas. Por favor verifique."

exit 1

done < $coords
