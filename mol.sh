#! /bin/bash

for i in *.pdbqt; do
base=$(basename $i .pdbqt)
obabel -ipdbqt $i -omol2 -O ${base}.mol2 -l 1

done 
